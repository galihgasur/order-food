package id.teknologi.orderfood

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import id.teknologi.orderfood.adapter.AdapterMakanan
import id.teknologi.orderfood.model.MakananModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), AdapterMakanan.AdapterMakananCallback {
    // inisiasi variable list
    var listMakanan = ArrayList<MakananModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // menambahkan data dummy ke variable list makanan
        listMakanan.add(MakananModel("Nama Makanan 1", "Deskripsi 1", R.drawable.food_1, 1000L))
        listMakanan.add(MakananModel("Nama Makanan 2", "Deskripsi 2", R.drawable.food_2, 2000L))
        listMakanan.add(MakananModel("Nama Makanan 3", "Deskripsi 3", R.drawable.food_3, 3000L))
        listMakanan.add(MakananModel("Nama Makanan 4", "Deskripsi 4", R.drawable.food_4, 4000L))
        listMakanan.add(MakananModel("Nama Makanan 5", "Deskripsi 5", R.drawable.food_1, 5000L))

        // lihat fun setupRecyclerAdapter()
        setupRecyclerAdapter()

        // atur ketik button order di tap
        btn_order.setOnClickListener {
            // membuat variabel list pesanan
            val listPesanan: ArrayList<MakananModel> = arrayListOf()

            // looping list makanan untuk check mana yang sudah ditambahkan
            listMakanan.forEach {
                // check kondisi
                if (it.jumlah > 0) {
                    listPesanan.add(it)
                }
            }

            if (listPesanan.size == 0) {
                Toast.makeText(this, "Harap pilih makanan terlebih dahulu", Toast.LENGTH_SHORT).show()
            } else {
                // buka result Activity
                val intent = Intent(this, ResultActivity::class.java)

                // pass variabel listPesanan melalui intent
                // (data class yang dikirim harus extend Serializable)
                intent.putExtra("pesanan", listPesanan)

                startActivity(intent)
            }


        }
    }

    fun setupRecyclerAdapter () {
        // inisiasi adapter
        val adapterMakanan = AdapterMakanan(this, listMakanan, this)

        // memasang adapter makanan ke recyclerview
        rv_list_makanan.adapter = adapterMakanan

        // menentukan recycler view layout manager
        val linearLayoutManager = LinearLayoutManager(
            this, LinearLayoutManager.VERTICAL, false)
        rv_list_makanan.layoutManager = linearLayoutManager

        // karena recycler di dalam scrollview
        rv_list_makanan.isNestedScrollingEnabled = true
    }

    override fun ketikaJumlahBerubah(position: Int) {
        var jumlah = 0
        var totalHarga = 0L
        for (makanan in listMakanan) {
            if (makanan.jumlah > 0) {
                jumlah = jumlah + 1
            }
            totalHarga = totalHarga + (makanan.jumlah * makanan.harga)
        }
        tv_total_pesanan.text = "${jumlah} pesanan | Rp ${totalHarga}"
    }
}
